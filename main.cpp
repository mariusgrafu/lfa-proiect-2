#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

string splitWord(vector<string> word, char *sep = ", "){
    string s = "";
    for(int i = 0; i < word.size(); ++i){
        s += word[i] + ((i != word.size() - 1)?sep:"");
    }
    return s;
}

class Link{
    string link_;
    int id_;

public:
    Link(){
        link_ = "";
        id_ = 0;
    }
    Link(string l, int i){
        link_ = l;
        id_ = i;
    }
    bool hasLink(string l){
        return (link_ == l);
    }
    int id(){
        return id_;
    }
};

class Node{
    string label_;
    bool last_;
    vector<Link> links_;

public:
    Node(){
        label_ = "";
        last_ = 0;
    }
    Node(string lbl, bool lst = 0){
        label_ = lbl;
        last_ = lst;
    }
    friend istream& operator>>(istream &in, Node &n){
        in >> n.label_ >> n.last_;
        return in;
    }
    vector<int> getLinks(const string l){
        vector<int> ret;
        for(int i = 0; i < links_.size(); ++i){
            if(links_[i].hasLink(l)) ret.push_back(links_[i].id());
        }
        return ret;
    }
    int gotoLink(const string l){
        for(int i = 0; i < links_.size(); ++i){
            if(links_[i].hasLink(l)) return links_[i].id();
        }
        return -1;
    }
    string label(){
        return label_;
    }
    void push_link(Link newLink){
        links_.push_back(newLink);
    }
    void last(bool val){
        last_ = val;
    }
    bool last(){
        return last_;
    }
};

class AFD{
    int q0_; // first state
    vector<string> alph_; // the alphabet
    vector<Node> nodes_; // the states

public:
    AFD(){
        q0_ = 0;
    }
    AFD(const string f_name){
        ifstream in(f_name.c_str());
        int states_count;
        in >> states_count;
        nodes_.resize(states_count);
        for(int i = 0; i < states_count; ++i){
            in >> nodes_[i];
        }
        in >> q0_;
        int alph_count;
        in >> alph_count;
        alph_.resize(alph_count);
        for(int i = 0; i < alph_count; ++i){
            in >> alph_[i];
        }
        string line, t_link;
        int x, y;
        in.ignore();
        while(getline(in, line)){
            if(in.bad()) break;
            stringstream ss(line);
            ss >> x >> y >> t_link;
            nodes_[x].push_link(Link(t_link, y));
        }
        in.close();
    }

    void q0(int start){
        q0_ = start;
    }

    void alph(vector<string> a){
        alph_ = a;
    }

    void nodes(vector<Node> n){
        nodes_ = n;
    }

    friend ostream& operator<<(ostream &out, AFD& a){
        for(int i = 0; i < a.nodes_.size(); ++i){
            for(int j = 0; j < a.alph_.size(); ++j){
                out << a.nodes_[i].label() << " -- " << a.alph_[j] << " --> ";
                int gotoL = a.nodes_[i].gotoLink(a.alph_[j]);
                if(gotoL != -1)
                    out << a.nodes_[gotoL].label() << "\n";
                else out << "X\n";
            }
        }
        return out;
    }

    void parseCmds(vector<string> cmds, int c_state){
        while(cmds.size() && cmds[0] == "L"){
                cmds.erase(cmds.begin());
        }
        if(!cmds.size()){
            if(nodes_[c_state].last()) cout << " - Correct";
            else cout << " - Incorrect";
            return;
        }
        int next_state = nodes_[c_state].gotoLink(cmds[0]);
        if(next_state == -1){
            cout << " - " << nodes_[c_state].label() << " doesn't link with " << cmds[0];
            return;
        }
        cmds.erase(cmds.begin());
        parseCmds(cmds, next_state);
    }

    void getInput(const string f_name){
        ifstream in(f_name.c_str());
        string line, cmd;
        while(getline(in, line)){
            if(in.bad()) break;
            stringstream ss(line);
            vector<string> cmds;
            while(ss >> cmd){
                if(ss.bad()) break;
                cmds.push_back(cmd);
            }
            cout << splitWord(cmds, ", ");
            parseCmds(cmds, q0_);
            cout << "\n";
        }
        in.close();
    }
};

class AFN{
    int q0_; // first state
    vector<string> alph_; // the alphabet
    vector<Node> nodes_; // the states

    bool found_;

public:
    AFN(){
        q0_ = 0;
    }
    AFN(const string f_name){
        ifstream in(f_name.c_str());
        int states_count;
        in >> states_count;
        nodes_.resize(states_count);
        for(int i = 0; i < states_count; ++i){
            in >> nodes_[i];
        }
        in >> q0_;
        int alph_count;
        in >> alph_count;
        alph_.resize(alph_count);
        for(int i = 0; i < alph_count; ++i){
            in >> alph_[i];
        }
        string line, t_link;
        int x, y;
        in.ignore();
        while(getline(in, line)){
            if(in.bad()) break;
            stringstream ss(line);
            ss >> x >> y >> t_link;
            nodes_[x].push_link(Link(t_link, y));
        }
        in.close();
    }

    void parseCmds(vector<string> cmds, int c_state){
        while(cmds.size() && cmds[0] == "L"){
                cmds.erase(cmds.begin());
        }
        if(!cmds.size()){
            if(nodes_[c_state].last()) found_ = nodes_[c_state].last();
            return;
        }
        vector<int> next_states = nodes_[c_state].getLinks(cmds[0]);
        cmds.erase(cmds.begin());
        for(int i = 0; i < next_states.size(); ++i){
            parseCmds(cmds, next_states[i]);
        }
    }

    vector<string> alph(){
        return alph_;
    }

    int q0(){
        return q0_;
    }

    int nodesCount(){
        return nodes_.size();
    }

    string getLabel(int i){
        if(i < 0 || i >= nodes_.size()) return "<no_label>";
        return nodes_[i].label();
    }

    Node getNode(int i){
        if(i >= 0 && i < nodes_.size()){
            return nodes_[i];
        }
    }

    void getInput(const string f_name){
        ifstream in(f_name.c_str());
        string line, cmd;
        while(getline(in, line)){
            if(in.bad()) break;
            stringstream ss(line);
            vector<string> cmds;
            while(ss >> cmd){
                if(ss.bad()) break;
                cmds.push_back(cmd);
            }
            found_ = 0;
            parseCmds(cmds, q0_);
            cout << splitWord(cmds, ", ");
            if(found_) cout << " - Correct";
            else cout << " - Incorrect";
            cout << "\n";
        }
        in.close();
    }

    string afd_getLabelNames(vector<int> nodeGroup){
        string ret = "[";
        for(int i = 0; i < nodeGroup.size(); ++i){
            ret += nodes_[nodeGroup[i]].label() + ((i != nodeGroup.size() - 1)?",":"");
        }
        ret += "]";
        return ret;
    }

    bool isLastInArray(vector<int> nodeGroup){
        for(int i = 0; i < nodeGroup.size(); ++i){
            if(nodes_[nodeGroup[i]].last()) return 1;
        }
        return 0;
    }

    void mergeGroups(vector<int> &a, vector<int> &b){
        for(int i = 0; i < b.size(); ++i){
            bool found = 0;
            for(int j = 0; j < a.size(); ++j){
                if(b[i] == a[j]){
                    found = 1;
                    break;
                }
            }
            if(!found) a.push_back(b[i]);
        }
    }

    void push_afdNode(vector<int> nodeGroup, vector<Node> &afd_nodes, int parent_id = -1, string l = ""){
        if(!nodeGroup.size()) return;
        string label = afd_getLabelNames(nodeGroup);
        int node_id = -1;
        bool alreadyParsed = 0;
        for(int i = 0; i < afd_nodes.size(); ++i){
            if(afd_nodes[i].label() == label){
                    node_id = i;
                    alreadyParsed = 1;
            }
        }
        if(node_id == -1){
            afd_nodes.push_back(Node(label, isLastInArray(nodeGroup)));
            node_id = afd_nodes.size() - 1;
        }
        if(parent_id != -1){
            afd_nodes[parent_id].push_link(Link(l, node_id));
        }
        if(alreadyParsed) return;
        for(int i = 0; i < alph_.size(); ++i){
            vector<int> newNodeGroup;
            for(int j = 0; j < nodeGroup.size(); ++j){
                vector<int> links = nodes_[nodeGroup[j]].getLinks(alph_[i]);
                mergeGroups(newNodeGroup, links);
            }
            push_afdNode(newNodeGroup, afd_nodes, node_id, alph_[i]);
        }
    }

    AFD& getAfd(){
        AFD *afd = new AFD;

        vector<int> nodeGroup;
        vector<Node> afd_nodes;
        nodeGroup.push_back(q0_);
        push_afdNode(nodeGroup, afd_nodes);

        afd->q0(0);
        afd->alph(alph_);
        afd->nodes(afd_nodes);

        return *afd;
    }
};

int main()
{
    //cout << "AFN:\n";
    AFN afn("afn2.in");
    //afn.getInput("afn_input.in");
    //cout << "\nAFD:\n";
    //AFD afd("afd.in");
    //afd.getInput("afd_input.in");
    cout << "AFN:\n";
    afn.getInput("afn_input2.in");
    cout << "AFD:\n";
    AFD afd = afn.getAfd();
    afd.getInput("afn_input2.in");
    cout << "\n\nAFD Links:\n";
    cout << afd;
    return 0;
}

